package application;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class ImageData implements Serializable {
	
	/**
	 * inner class to represent tags values and categories
	 */
	public class Tuple implements Serializable{
		public String tag;
		public String cat;
		public Tuple(String tag, String cat) {
			this.tag = tag;
			this.cat = cat;
		}
		/**
		 * gets tag of category/tag tuple
		 * @return tag
		 */
		public String getTag()
		{
			return tag;
		}
		/**
		 * gets category of category/tag tuple
		 * @return category
		 */
		public String getCat()
		{
			return cat;
		}
		
		public boolean equals(Tuple t) {
			return this.cat.equals(t.cat) && this.tag.equals(t.tag);
		}
	}
	/**
	 * array list of all tags for this image
	 */
	private ArrayList<Tuple> tags;
	/**
	 * date at which instance was created
	 */
	private Date date;
	/**
	 * user written caption string
	 */
	private String caption = "";
	/**
	 * file path to image
	 */
	private String imagePath;

	public ImageData(String imagePath) {
		Calendar c = new GregorianCalendar(); 
		c.set(Calendar.MILLISECOND,0);
		date = c.getTime();
		
		setTags(new ArrayList<Tuple>());
		
		this.imagePath = imagePath;
	}
	@Override
	public String toString() {
		return this.imagePath;
	}

	
	public Date getDate() {
		return date;
	}
	
	
	public String getCaption(){
		return this.caption;
	}
	
	public void setCaption(String caption){
		this.caption = caption;
	}
	

	public String getImagePath() {
		return imagePath;
	}

	
	public ArrayList<Tuple> getTags() {
		return tags;
	}

	
	public void setTags(ArrayList<Tuple> tags) {
		this.tags = tags;
	}
}
