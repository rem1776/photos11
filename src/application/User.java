package application;

import java.io.Serializable;
import java.util.ArrayList;

public class User implements Serializable {

		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		/**
		 * username of user account
		 */
		private String username;
		/**
		 * list of all of the user's albums
		 */
		private ArrayList<Album> albumList;
		
		/**
		 * represents a user's album with name and list of images
		 */
		public class Album implements Serializable {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;
			public String name;
			public ArrayList<ImageData> images;
			
			public Album(String name) {
				this.name = name;
				images = new ArrayList<ImageData>();
			}
			
		}
		/**
		 * Creates a new User object with the given username and an empty list of albums
		 * @param username username of user to be created
		 */
		public User(String username) {
			this.username = username;
			albumList = new ArrayList<Album>();
		}
		
		/**
		 * Get User's username
		 * @return username of this User instance
		 */
		public String getUsername() {
			return username;
		}
		/**
		 * Get Users current list of albums
		 * @return ArrayList of user's albums
		 */
		public ArrayList<Album> getAlbumList() {
			return albumList;
		}
		
		/**
		 * Set users list of current albums
		 * @param arr new list of albums to set for this user
		 */
		public void setAlbumList(ArrayList<Album> arr) {
			this.albumList = arr;
		}
		/**
		 * Checks if username of this user and the given user is the same
		 * @param u username to check
		 * @return true for matching usernames, false otherwise
		 */
		public boolean equals(User u) {
			return this.username.equals(u.username);
			
		}
		
}
