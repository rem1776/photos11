package application;
	
import view.AdminController;
import view.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader; 
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;


public class Photos extends Application {
	
	/**
	 * Initializes window and starts LoginController
	 * @param primaryStage main application window
	 */
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loginLoader = new FXMLLoader();
			loginLoader.setLocation(getClass().getResource("/view/login.fxml")); 
			LoginController lcon = new LoginController();
			loginLoader.setController(lcon);
			
			
			
			Pane root = (Pane)loginLoader.load();
			lcon.start();
			Scene scene = new Scene(root,600,400);
			primaryStage.setScene(scene);
			primaryStage.show();
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
