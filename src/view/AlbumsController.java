package view;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*; 
import javafx.collections.FXCollections;
import javafx.collections.ObservableList; 
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.Stage;
import javafx.scene.control.ListView;

import javax.swing.JOptionPane;

import application.ImageData;
import application.ImageData.Tuple;
import application.User;
import application.User.Album;

/**
 * Controller class for albums.fxml
 * Handles opening, adding, editing, and deleting albums and searching User's pictures
 * 
 */
public class AlbumsController {
	@FXML Button openButton;
	@FXML Button addButton;
	@FXML Button editButton;
	@FXML Button deleteButton;
	@FXML Button logoutButton;
	@FXML Button searchTagButton;
	@FXML Button searchDateButton;
	@FXML TextField searchField;
	@FXML ListView<String> albumList;
	@FXML DatePicker datePicker1;
	@FXML DatePicker datePicker2;

	/**
	 * list of album names to be displayed on albumList
	 */
	private ObservableList<String> obsList;

	/**
	 * static instance 
	 */
	static Album currAlbum;
	
	/**
	 * Handles button actions when any button is pressed
	 * @param e Action event from a button being pressed
	 */
	private void buttPush(ActionEvent e) {
		
		Button b = (Button)e.getSource();
		System.out.print(b);
		
		int i = albumList.getSelectionModel().getSelectedIndex();

		//TODO go to image view
		if (b == openButton) {
			if(i > -1) {
				
				currAlbum = LoginController.currUser.getAlbumList().get(i);
				if(currAlbum.images == null) {
					currAlbum.images = new ArrayList<ImageData>();
				}
				
				try {
					
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/image.fxml"));
					Parent root;
					ImageController ic = new ImageController();
					fxmlLoader.setController(ic);
					root = (Parent) fxmlLoader.load();
					ic.start();
					
					Scene scene = new Scene(root, 600, 400);
					Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
					primStage.setScene(scene);
					primStage.show();
					
				} catch(IOException excp) {
					System.out.print(excp + "\nCouldn't load login page");
				}
			}
			
		}
		else if (b == addButton) {
			
			TextInputDialog td = new TextInputDialog("Album Name");
			td.setHeaderText("Enter new Album Name");
			td.setContentText("");
			
			String albName = "";
			Optional<String> res = td.showAndWait();
			if(res.isPresent() && !res.get().equals("") && !checkDuplicate(LoginController.currUser.new Album(res.get()))) {
				albName = res.get();
				//add new album entry for current user
				LoginController.currUser.getAlbumList().add(LoginController.currUser.new Album(albName));
				obsList.add(albName);
			}
			else {
				JOptionPane.showMessageDialog(null, "Duplicate/No album name given");
			}
			
			
			
		}
		else if (b == deleteButton){
			
			if(i > -1) {
				System.out.println(i);
				LoginController.currUser.getAlbumList().remove(i);
				obsList.remove(i);
			}
			
		}
		else if (b == editButton)
		{
			if(i > -1) {
								
				TextInputDialog td = new TextInputDialog("Album Name");
				td.setHeaderText("Enter new Album Name");
				td.setContentText("");
				
				String albName = "";
				Optional<String> res = td.showAndWait();
				if(res.isPresent() && res.get() != "" && !checkDuplicate(LoginController.currUser.new Album(res.get()))) {
					albName = res.get();
					LoginController.currUser.getAlbumList().get(i).name = albName; 
					obsList.set(i, albName);
				}
				else {
					JOptionPane.showMessageDialog(null, "Duplicate/No album name given");
				}
				
			}
		}
		else if(b == logoutButton) {
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/login.fxml"));
				Parent root;
				LoginController lc = new LoginController();
				fxmlLoader.setController(lc);
				root = (Parent) fxmlLoader.load();
				lc.start();
				
				Scene scene = new Scene(root, 600, 400);
				Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
				primStage.setScene(scene);
				primStage.show();
			} catch(IOException excp) {
				System.out.print(excp + "\nCouldn't load login page");
			}
		}
		else if (b == searchTagButton) {
			
			String search = searchField.getText();
		
			if(!search.contains("=") && !search.equals("")) {
				JOptionPane.showMessageDialog(null, "Invalid search");
			}
			else{
				
				currAlbum = getSearchPictures(search);
				
				//load the image view page
				try {
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/image.fxml"));
					Parent root;
					ImageController ic = new ImageController();
					fxmlLoader.setController(ic);
					root = (Parent) fxmlLoader.load();
					ic.start();
					
					Scene scene = new Scene(root, 600, 400);
					Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
					primStage.setScene(scene);
					primStage.show();
				} catch(IOException excp) {
					System.out.print(excp + "\nCouldn't load login page");
				}
			}	
		}
		else if (b == searchDateButton) {
			if(datePicker1.getValue() != null && datePicker2.getValue() != null) {
				System.out.println("Searching by date");
				
				Date d1 = Date.from( datePicker1.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());
				Date d2 = Date.from( datePicker2.getValue().atStartOfDay(ZoneId.systemDefault()).toInstant());

				currAlbum = getDatePictures(d1, d2);
				
				//load the image view page
				try {
					FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/image.fxml"));
					Parent root;
					ImageController ic = new ImageController();
					fxmlLoader.setController(ic);
					root = (Parent) fxmlLoader.load();
					ic.start();
					
					Scene scene = new Scene(root, 600, 400);
					Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
					primStage.setScene(scene);
					primStage.show();
				} catch(IOException excp) {
					System.out.print(excp + "\nCouldn't load login page");
				}
			}
		}
		else {
			System.out.println("Wow you pressed a button that doesn't exist.");
		}
		
		try {
	         FileOutputStream fileOut = new FileOutputStream("data.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(LoginController.userList);
	         out.close();
	         fileOut.close();
	      } catch (IOException io) {
	         io.printStackTrace();
	      }
	}
	
	/**
	 * Sets up controller for when scene is opened
	 */	 
	public void start() {
		obsList = FXCollections.observableArrayList();
		
		logoutButton.setOnAction(a -> buttPush(a));
		addButton.setOnAction(a -> buttPush(a));
		deleteButton.setOnAction(a -> buttPush(a));
		openButton.setOnAction(a -> buttPush(a));
		editButton.setOnAction(a -> buttPush(a));
		searchTagButton.setOnAction(a -> buttPush(a));
		searchDateButton.setOnAction(a -> buttPush(a));
		
		for(Album a : LoginController.currUser.getAlbumList())
		{
			obsList.add(a.name);
		}
		
		System.out.println(obsList.toString());
		albumList.setItems(obsList);
		
		//accList.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> showItem());

	}
	/**
	 * Checks for any duplicate album names in list
	 * @param a album to check duplicates of
	 * @return true if duplicate is found, false otherwise
	 */
	private boolean checkDuplicate(Album a) {
		for(String s: obsList) {
			if(a.name.equals(s))
				return true;
		}
		return false;
	}
	/**
	 * Finds any picture in any of the current users albums that has date in between the two provided
	 * @param d date to search for
	 * @return an album of search results containing all pictures with the given date
	 */
	private Album getDatePictures(Date d1, Date d2) {
		
		if(d1.after(d2)) {
			Date temp = d1;
			d1 = d2;
			d2 = temp;
		}
		
		Album res = LoginController.currUser.new Album("search results");
		for (Album a : LoginController.currUser.getAlbumList()) {
			for (ImageData img : a.images) {
				Date imgDate = img.getDate();
				
				d2.setTime(d2.getTime() + 86400000);
				if( imgDate.after(d1) && imgDate.before(d2)) {
					res.images.add(img);
				}
			}
		}
		return res;
	}
	/**
	 * 
	 * @param search the raw search string with tag names and values 
	 * @return An Album instance containing all images that match the search conditions
	 */
	private Album getSearchPictures(String search) {
		
		
		Album res = LoginController.currUser.new Album("search results");
		res.images = new ArrayList<ImageData>();
		search = search.toLowerCase();
		
		
		if(search.contains(" and ")) {
			
			int ind = search.indexOf(" and ");			
			String[] pair1 = parseSearch(search.substring(0, ind));
			String[] pair2 = parseSearch(search.substring(ind+5));
			
			boolean found = false;
			//oof
			for (Album a : LoginController.currUser.getAlbumList()) {
				for (ImageData img : a.images) {
					for (int i=0 ;i < img.getTags().size()-1 && img.getTags().size() > 1; i++) {
	
						Tuple t1 = img.getTags().get(i);			
						for(int j=i+1;j< img.getTags().size(); j++) {
							
							Tuple t2 = img.getTags().get(j);
							if(t1.cat.trim().equals(pair1[0]) && t1.tag.trim().equals(pair1[1]) && t2.cat.equals(pair2[0]) && t2.tag.equals(pair2[1])) {
								res.images.add(img);
								found = true;
								break;
							}
						}
						if(found)
							break;
					}
				}
			}
			 
		}
		else if (search.contains(" or ")) {
			int ind = search.indexOf(" or ");			
			String[] pair1 = parseSearch(search.substring(0, ind));
			String[] pair2 = parseSearch(search.substring(ind+4));

			for (Album a : LoginController.currUser.getAlbumList()) {
				for (ImageData img : a.images) {
					for (int i=0 ;i < img.getTags().size(); i++) {
						Tuple t = img.getTags().get(i);		
						if( (t.cat.trim().equals(pair1[0]) && t.tag.trim().equals(pair1[1])) || (t.cat.equals(pair2[0]) && t.tag.equals(pair2[1])) ) {
							res.images.add(img);
							break;
						}	
					}
				}
			}
		}
		else {
			String[] pair = parseSearch(search);
			System.out.println(pair[0] + pair[1]);
			//oof
			for (Album a : LoginController.currUser.getAlbumList()) {
				for (ImageData img : a.images) {
					for (int i=0 ;i < img.getTags().size(); i++) {
	
						Tuple t = img.getTags().get(i);		
						
						if((t.cat.trim().equals(pair[0]) && t.tag.trim().equals(pair[1]))) {
							res.images.add(img);
							break;
						}	
					}
				}
			}
		}
		return res;
	}
	/**
	 * Seperates search terms into category and tag strings
	 * @param s - search term containing a '=' with a category before and tag after
	 * @return a string array with category at i=0 and tag at i=1 
	 */
	private String[] parseSearch(String s) {
		String[] res = new String[2];
		int i = s.indexOf("=");
		res[0] = s.substring(0, i).trim();
		res[1] = s.substring(i+1).trim();
		return res;
	}
	
	
}