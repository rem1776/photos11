package view;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import application.ImageData;

import java.util.*; 

import javafx.collections.FXCollections;
import javafx.collections.ObservableList; 
import javafx.scene.control.ListCell;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.control.ListView;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

import java.util.ArrayList;

import application.ImageData;
import application.User;

/**
 * Controller for image.fxml
 * Handles cataloging and display of images within an album
 * allows for movement of images between albums
 */
public class ImageController {
	@FXML Button saveAlbum;
	@FXML Button addButton;
	@FXML Button copyButton;
	@FXML Button moveButton;
	@FXML Button backButton;
	@FXML Button forwardButton;
	@FXML Button deleteButton;
	@FXML Button logoutButton;
	@FXML Button delTagButton;
	@FXML Button saveTagButton;
	@FXML Button saveCapButton;
	@FXML TextField capField;
	@FXML TextField tagField;
	@FXML Label dateLabel;
	@FXML ListView<ImageData> imageList;
	@FXML ListView<String> tagList;
	@FXML ImageView imageView;
	/**
	 * Observable list containing ImageData objects for display
	 */
	private ObservableList<ImageData> imageObsList;
	/**
	 * Observable list containing image tags in string format
	 */
	private ObservableList<String> tagObsList;
	/**
	 * Image being hovered in the listview
	 */
	private ImageData currImage;
	
	/**
	 * Handles all potential button presses by user
	 * @param e ActionEvent caused by button press
	 */
	public void buttPush(ActionEvent e) {
		
		Button b = (Button)e.getSource();
		System.out.print(b);

		if (b == addButton) 
		{
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Choose image to add");
			Stage fileStage = new Stage();
			File file = fileChooser.showOpenDialog(fileStage);
			AlbumsController.currAlbum.images.add(new ImageData(file.getAbsolutePath()));
			updateImageList();
		}
		else if(b == logoutButton) {
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/login.fxml"));
				Parent root;
				LoginController lc = new LoginController();
				fxmlLoader.setController(lc);
				root = (Parent) fxmlLoader.load();
				lc.start();
				
				Scene scene = new Scene(root, 600, 400);
				Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
				primStage.setScene(scene);
				primStage.show();
			} catch(IOException excp) {
				System.out.print(excp + "\nCouldn't load login page");
			}
		}
		else if(currImage != null)
		{
			if (b == deleteButton)
			{
				AlbumsController.currAlbum.images.remove(currImage);
				updateImageList();
				tagObsList.clear();
				capField.setText("");
				imageView.setImage(null);
				currImage = null;
				
			}
			else if (b == copyButton)
			{
				moveImage();
			}
			else if (b == moveButton)
			{
				if(moveImage())
				{
					AlbumsController.currAlbum.images.remove(currImage);
					updateImageList();
					tagObsList.clear();
					capField.setText("");
					imageView.setImage(null);
					currImage = null;
				}
			}
			else if (b == delTagButton)
			{
				String toDel = tagList.getSelectionModel().getSelectedItem();
				int x;
				for(x= 0; x < toDel.length(); x++)
				{
					if(toDel.charAt(x) == '=')
					{
						break;
					}
				}
				
				String cat = toDel.substring(0, x-1);
				String tag = toDel.substring(x+2);
				
				for(ImageData.Tuple t : currImage.getTags())
				{
					if(t.getTag().equals(tag) && t.getCat().equals(cat))
					{
						currImage.getTags().remove(t);
						break;
					}
				}
				updateTagList();
			}
			else if(b == saveTagButton)
			{
				Alert a = new Alert(AlertType.ERROR);
				String newTag = tagField.getText();
				if(newTag.contains(" = "))
				{
					
					int x;
					for(x= 0; x < newTag.length(); x++)
					{
						if(newTag.charAt(x) == '=')
						{
							break;
						}
					}
					
					String cat = newTag.substring(0, x - 1);
					String tag = newTag.substring(x + 2);
					boolean found = false;
					for(ImageData.Tuple t : currImage.getTags())
					{
						if(t.tag.equals(tag) && t.cat.equals(cat))
						{
							found = true;
						}
					}
					if(found)
					{
						a.setContentText("No duplicate tags");
						a.show();
					}
					else
					{
						currImage.getTags().add(currImage.new Tuple(tag, cat));
						updateTagList();
					}
				}
				else
				{
					a.setContentText("Tag must be of format '<category> = <tag>'");
					a.show();
				}
			}
			else if (b == saveCapButton)
			{
				currImage.setCaption(capField.getText());
				updateImageList();
			}
			else if(b == forwardButton)
			{
				int i = imageList.getSelectionModel().getSelectedIndex();
				
				if(i == imageObsList.size() - 1)
				{
					imageList.getSelectionModel().select(0);
				}
				else 
				{
					imageList.getSelectionModel().select(i + 1);
				}
			}
			else if(b == backButton)
			{
				int i = imageList.getSelectionModel().getSelectedIndex();
				
				if(i == 0)
				{
					imageList.getSelectionModel().select(imageObsList.size() - 1);
				}
				else 
				{
					imageList.getSelectionModel().select(i - 1);
				}
			}
		}
		else if(b == saveAlbum)
		{
			Alert c = new Alert(AlertType.ERROR);
			TextInputDialog td = new TextInputDialog("Album Name");
			td.setHeaderText("Please name your new album.");
			td.setContentText("");
			Optional<String> copyTo = td.showAndWait();
			if(copyTo.isPresent())
			{
				String newAlbum = copyTo.get();
				boolean found = false;
				for(User.Album a : LoginController.currUser.getAlbumList())
				{
					if(a.name.equals(newAlbum))
					{
						found = true;
					}
				}
				
				if(found)
				{
					c.setContentText("Album already exists with that name."); 
					c.show();
				}
				else
				{
					AlbumsController.currAlbum.name = newAlbum;
					LoginController.currUser.getAlbumList().add(AlbumsController.currAlbum);
					saveAlbum.setVisible(false);
				}
			}
			
		}
		else {
			System.out.println("Wow you pressed a button that doesn't exist.");
		}
		try {
	         FileOutputStream fileOut = new FileOutputStream("data.ser");
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(LoginController.userList);
	         out.close();
	         fileOut.close();
	      } catch (IOException io) {
	         io.printStackTrace();
	      }
	}
	
	/**
	 * sets up all elements of the image window, loads up album
	 */
	public void start() {
		imageObsList = FXCollections.observableArrayList();
		tagObsList = FXCollections.observableArrayList();
		
		saveAlbum.setOnAction(a -> buttPush(a));
		logoutButton.setOnAction(a -> buttPush(a));
		addButton.setOnAction(a -> buttPush(a));
		copyButton.setOnAction(a -> buttPush(a));
		moveButton.setOnAction(a -> buttPush(a));
		deleteButton.setOnAction(a -> buttPush(a));
		backButton.setOnAction(a -> buttPush(a));
		forwardButton.setOnAction(a -> buttPush(a));
		delTagButton.setOnAction(a -> buttPush(a));
		saveTagButton.setOnAction(a -> buttPush(a));
		saveCapButton.setOnAction(a -> buttPush(a));
		
		if(AlbumsController.currAlbum.name.equals("search results"))
		{
			saveAlbum.setVisible(true);
		}
		else
		{
			saveAlbum.setVisible(false);
		}
		
		imageList.setCellFactory(listView -> new ListCell<ImageData>() {
            @Override
            protected void updateItem(ImageData img, boolean empty) {
                super.updateItem(img, empty);

                if (empty) {
                    setGraphic(null);
                } else {

                    HBox hBox = new HBox(5);
                    hBox.setAlignment(Pos.CENTER);
                    ImageView image = new ImageView();
         
                    if(img.getImagePath().substring(0, 4).equals("file"))
            		{
            			image.setImage(new Image(img.getImagePath(), 50, 50, false, false));
            		}
            		else
            		{
            			File file = new File(img.getImagePath());
            			
            			image.setImage(new Image(file.toURI().toString(), 50, 50, false, false));
            		
            		}
                    hBox.getChildren().addAll(
                            image,
                            new Label(img.getCaption())
                    );
                    setGraphic(hBox);
                }
            }
        });
		updateImageList();
		
		imageList.setItems(imageObsList);
		tagList.setItems(tagObsList);
		
		imageList.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> showItem());

	}
	
	/**
	 * configures image and information field when an image is selected from the ListView
	 */
	public void showItem()
	{
		ImageData img = imageList.getSelectionModel().getSelectedItem();
		for(ImageData i : AlbumsController.currAlbum.images)
		{
			if(i == img)
			{
				currImage = i;
				break;
			}
		}
		
		updateTagList();
		
		capField.setText(currImage.getCaption());
		
		if(currImage.getImagePath().substring(0, 4).equals("file"))
		{
			imageView.setImage(new Image(currImage.getImagePath()));
		}
		else
		{
			File file = new File(currImage.getImagePath());
			
			imageView.setImage(new Image(file.toURI().toString()));
		
		}
		dateLabel.setText(currImage.getDate().toString());
	}
	
	/**
	 * updates image listview
	 */
	public void updateImageList()
	{
		imageObsList.clear();
		for(ImageData i : AlbumsController.currAlbum.images)
		{
			imageObsList.add(i);
		}
	}
	/**
	 * updates tag list
	 */
	public void updateTagList()
	{
		tagObsList.clear();
		
		for(ImageData.Tuple tag : currImage.getTags())
		{
			tagObsList.add(tag.getCat() + " : " + tag.getTag());
		}
	}
	/**
	 * Moves an image to another album which the user specifies
	 * @return true if image was able to be moved, false otherwise due to album not existing
	 */
	public boolean moveImage()
	{
		Alert a = new Alert(AlertType.ERROR);
		TextInputDialog td = new TextInputDialog("Copy to Album");
		td.setHeaderText("Enter name of album you'd like to copy to.");
		td.setContentText("");
		Optional<String> copyTo = td.showAndWait();
		if(copyTo.isPresent())
		{
			String copyString = copyTo.get();
			if(copyString.equals(AlbumsController.currAlbum.name))
			{
				a.setContentText("Cannot copy to current album"); 
				a.show();
				return false;
			}
			else 
			{
				
				boolean found = false;
				for(User.Album alb : LoginController.currUser.getAlbumList())
				{
					if(alb.name.equals(copyString))
					{
						found = true;
						alb.images.add(currImage);
						return true;
					}
				}
				if(!found)
				{
					a.setContentText("Specified folder does not exist.");
					a.show();
					return false;
				}
			}
		}
		return false;
	}
}
