package view;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.*; 
import javafx.collections.FXCollections;
import javafx.collections.ObservableList; 
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.control.ListView;
import java.util.ArrayList;

import application.User;

/**
 * Controller for admin.fxml page
 * Handles adding and deleting user accounts
 */
public class AdminController {
	@FXML Button addButton;
	@FXML Button delButton;
	@FXML Button logoutButton;
	@FXML TextField userField;
	@FXML ListView<String> accList;
	
	/**
	 * List to display user names in list view
	 */
	private ObservableList<String> obsList;	
	
	/**
	 * called when buttons are pressed and
	 * handles adding and deleting user accounts then saves list on edits
	 * @param e - Action event caused by a button press
	 */
	public void buttPush(ActionEvent e) {
		
		Button b = (Button)e.getSource();
		System.out.print(b);

		if (b == addButton) {
			System.out.println("Add button pressed.");
			boolean unique = true;
			if(userField.getText() != null)
			{
				for(User u: LoginController.userList)
				{
					if(u.getUsername().equals(userField.getText()))
					{
						unique = false;
					}
				}
				
				if(unique)
				{
					if(userField.getText().trim().equals("")) {
						userField.setText("Pleae enter valid account name.");

					}
					else {
						LoginController.userList.add(new User(userField.getText()));
						obsList.add(userField.getText());
						System.out.println("User added.");
					}
				}
				else
				{
					userField.setText("This user already exists.");
				}				
			}
			try {
		         FileOutputStream fileOut = new FileOutputStream("data.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(LoginController.userList);
		         out.close();
		         fileOut.close();
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
		}
		else if (b == delButton){
			String toDel = accList.getSelectionModel().getSelectedItem();
			if(toDel.equals("stock"))
			{
				userField.setText("You may not delete this user.");
			}
			else
			{
				for(User u: LoginController.userList)
				{
					if(u.getUsername().equals(toDel))
					{
						obsList.remove(toDel);
						LoginController.userList.remove(u);
						break;
					}
				}
			}
			
			try {
		         FileOutputStream fileOut =
		         new FileOutputStream("data.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(LoginController.userList);
		         out.close();
		         fileOut.close();
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
		
		}
		else if(b == logoutButton) {
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/login.fxml"));
				Parent root;
				LoginController lc = new LoginController();
				fxmlLoader.setController(lc);
				root = (Parent) fxmlLoader.load();
				lc.start();
				
				Scene scene = new Scene(root, 600, 400);
				Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
				primStage.setScene(scene);
				primStage.show();
			} catch(IOException excp) {
				System.out.print(excp + "\nCouldn't load login page");
			}
		}
		else {
			System.out.println("Wow you pressed a button that doesn't exist.");
		}
	}
	
	/**
	 * sets up controller and adds listeners for buttons
	 */
	public void start() {
		obsList = FXCollections.observableArrayList();
		
		logoutButton.setOnAction(a -> buttPush(a));
		addButton.setOnAction(a -> buttPush(a));
		delButton.setOnAction(a -> buttPush(a));
		
		
		for(User u : LoginController.userList)
		{
			obsList.add(u.getUsername());
		}
		
		System.out.println(obsList.toString());
		accList.setItems(obsList);  //this line alwys throws null pointer excp even tho obsList isnt null
		
		//accList.getSelectionModel().selectedIndexProperty().addListener((obs, oldVal, newVal) -> showItem());

	}
	
}