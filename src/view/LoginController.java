package view;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

import javax.swing.JOptionPane;

import application.ImageData;
import application.ImageData.Tuple;
import application.User;
import application.User.Album;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList; 
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

/**
 * Controller for login.fxml
 * Handles logging into admin or user accounts
 * Stores list of users and current logged in userg
 */
public class LoginController {
	@FXML Button loginButton;
	@FXML TextField loginField;
	@FXML Pane root;
	
	/**
	 * List of all registered users in the application 
	 */	
	static ArrayList<User> userList;
	
	/**
	 * current user signed in to application
	 */
	static User currUser;
	
	/**
	 * Handles actions for when login button is pressed
	 * @param e ActionEvent caused by button press
	 */
	public void buttPush(ActionEvent e) {
		
		String usr = loginField.getText();
		
		if(usr.equals("admin")) {
			System.out.println("Go to admin page");
			 
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/admin.fxml"));
				Parent root;
				AdminController ac = new AdminController();
				fxmlLoader.setController(ac);
				root = (Parent) fxmlLoader.load();
				ac.start();
				
				Scene scene = new Scene(root, 600, 400);
				Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
				primStage.setScene(scene);
				primStage.show();

			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
		else if (getUsr(usr) != null){
			
			System.out.println("Go to usr");
			currUser = getUsr(usr);
			
			try {
				FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/albums.fxml"));
				Parent root;
				AlbumsController alc = new AlbumsController();
				fxmlLoader.setController(alc);
				root = (Parent) fxmlLoader.load();
				alc.start();
				
				Scene scene = new Scene(root, 600, 400);
				Stage primStage = (Stage)((Node)e.getSource()).getScene().getWindow();
				primStage.setScene(scene);
				primStage.show();

			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
		}
		else {
			JOptionPane.showMessageDialog(null, "Username not found.");
		}
		
		loginField.clear();
	}
	/**
	 * Sets up controller for when scene is opened
	 */
	public void start() {
		
		if(userList == null)
		{
			userList = new ArrayList<User>();
			User stck = new User("stock");
			stck.getAlbumList().add(stck.new Album("stock"));
			
			ArrayList<ImageData> imgs = new ArrayList<ImageData>();
			imgs.add(new ImageData("file:data/stock1.jpg"));
			imgs.add(new ImageData("file:data/stock2.jpg"));
			imgs.add(new ImageData("file:data/stock3.jpg"));
			imgs.add(new ImageData("file:data/stock4.png"));
			imgs.add(new ImageData("file:data/stock5.jpg"));
			stck.getAlbumList().get(0).images = imgs;
			userList.add(stck);
		}
		ArrayList<User> loadedList = null;
		
		
	    try {
	    	FileInputStream fileIn = new FileInputStream("data.ser");
	    	ObjectInputStream in = new ObjectInputStream(fileIn);
	    	loadedList  = (ArrayList<User>) in.readObject();
	        in.close();
	        fileIn.close();
		        
	        userList = loadedList;
	    } catch (IOException i) {
	        i.printStackTrace();
	        return;
	    } catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		    
		    
		
		loginButton.setOnAction(a -> buttPush(a) );
		
		
		
	}
	
	/**
	 * gets a user from the Application's user list by username
	 * @param username
	 * @return User instance with the given username
	 */
	public User getUsr(String username) {
		User t = new User(username);
		for(User u: userList) {
			if(u.equals(t))
				return u;
		}
		return null;
	}
	
	
}
